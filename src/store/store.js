import Vuex from "vuex";
import Vue from "vue";
import orderStore from "./orderStore";
import authStore from "./authStore";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    orderStore: orderStore,
    authStore: authStore
  }
});
