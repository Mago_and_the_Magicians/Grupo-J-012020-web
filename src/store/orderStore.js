const orderStore = {
  // You can use it as state property
  state: {
    order: {
      storeOrders: [],
      state: "CART",
      totalAmount: 0,
      totalDiscount: 0,
      total: 0
    }
  },

  getters: {
    getLineItems: state => {
      return state.order.storeOrders.reduce(
        (array, stateOrder) =>
          array.concat(
            stateOrder.lineItems.map(lineItem => {
              return {
                code: lineItem.storeVariant.code,
                quantity: lineItem.quantity
              };
            })
          ),
        []
      );
    },
    getShippingMethodsInfo: state => {
      return state.order.storeOrders.map(storeOrder => {
        return {
          code: storeOrder.code,
          shippingMethod: storeOrder.shippingMethod.option,
          additionalInfo: storeOrder.shippingMethod.additional
        };
      });
    },
    getShippingMethod: state => storeOrderCode => {
      return state.order.storeOrders.find(so => so.code === storeOrderCode);
    },
    storeVariantQuantity: state => storeVariant => {
      let storeOrder = state.order.storeOrders.find(
        el => el.code === storeVariant.store.code
      );
      if (storeOrder) return 0;
      let lineItem = storeOrder.lineItems.find(
        el => el.code === storeVariant.code
      );
      return lineItem ? 0 : lineItem.quantity;
    },
    order: state => {
      return state.order;
    },
    storeOrders: state => {
      return state.order.storeOrders;
    },
    totalAmount: state => {
      return state.order.total;
    }
  },
  mutations: {
    MODIFY_PRODUCT_QUANTITY(state, lineItemChange) {
      let storeToFind = lineItemChange.store.code;

      let storeOrder = state.order.storeOrders.find(
        so => so.store.code === storeToFind
      );

      if (!storeOrder) {
        state.order.storeOrders.push({
          code: storeToFind,
          store: lineItemChange.store,
          lineItems: [lineItemChange],
          total: lineItemChange.total
        });
        state.order.total = state.order.total + lineItemChange.total;
        localStorage.setItem("order", JSON.stringify(state.order));
        return;
      }
      let lineItem = storeOrder.lineItems.find(
        el => el.code === lineItemChange.code
      );
      if (!lineItem) {
        storeOrder.lineItems.push(lineItemChange);
        localStorage.setItem("order", JSON.stringify(state.order));
        return;
      }

      storeOrder.lineItems.splice(
        storeOrder.lineItems.indexOf(lineItem),
        1,
        lineItemChange
      );
      storeOrder.total =
        storeOrder.total - lineItem.total + lineItemChange.total;
      state.order.total =
        state.order.total + -lineItem.total + lineItemChange.total;
      localStorage.setItem("order", JSON.stringify(state.order));
    },
    SELECT_SHIPPING_METHOD: (state, shippingSelectionOption) => {
      let storeOrder = state.order.storeOrders.find(
        so => so.store.code === shippingSelectionOption.orderCode
      );
      storeOrder.shippingMethod = {
        option: shippingSelectionOption.shippingOption
      };
      localStorage.setItem("order", JSON.stringify(state.order));
    },
    ADD_ADDITIONAL_INFO_TO_SHIPPING: (state, additionalShippingMethod) => {
      let storeOrder = state.order.storeOrders.find(
        so => so.store.code === additionalShippingMethod.orderCode
      );
      storeOrder.shippingMethod.additional =
        additionalShippingMethod.additional;
      localStorage.setItem("order", JSON.stringify(state.order));
    },
    CHANGE_STATE_TO(state, orderstate) {
      state.order.state = orderstate;
      localStorage.setItem("order", JSON.stringify(state.order));
    },
    PERSIST_ORDER: (state, order) => {
      state.order = order;
      localStorage.setItem("order", JSON.stringify(state.order));
    },
    INITIALIZE_ORDER: state => {
      let orderToParse = localStorage.getItem("order");
      if (orderToParse) state.order = JSON.parse(orderToParse);
    },
    DELETE_ORDER: state => {
      state.order = {
        storeOrders: [],
        state: "CART",
        totalAmount: 0,
        totalDiscount: 0,
        total: 0
      };
    }
  },

  actions: {
    modifyProductQuantity(context, lineItemChange) {
      context.commit("MODIFY_PRODUCT_QUANTITY", lineItemChange);
    },
    selectShippingMethod(context, shippingSelectionOption) {
      context.commit("SELECT_SHIPPING_METHOD", shippingSelectionOption);
    },
    addAdditionalInfoToShipping(context, additionalShippingMethod) {
      context.commit(
        "ADD_ADDITIONAL_INFO_TO_SHIPPING",
        additionalShippingMethod
      );
    },
    changeStateTo(context, orderstate) {
      context.commit("CHANGE_STATE_TO", orderstate);
    },
    persistOrder(context, order) {
      context.commit("PERSIST_ORDER", order);
    },
    deleteOrder(context) {
      context.commit("DELETE_ORDER");
    }
  }
};
export default orderStore;
