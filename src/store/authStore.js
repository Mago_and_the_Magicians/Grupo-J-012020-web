const authStore = {
  state: {
    user: null
  },
  mutations: {
    LOGIN_USER(state, user) {
      state.user = user;
      localStorage.setItem("user", JSON.stringify(user));
    },
    INITIALIZE_USER: state => {
      let userToParse = localStorage.getItem("user");
      if (userToParse) state.user = JSON.parse(userToParse);
    }
  },
  actions: {
    persistLogin(context, userData) {
      context.commit("LOGIN_USER", userData);
    }
  },
  getters: {
    userLoged: state => {
      return state.user !== null;
    },
    authToken: state => {
      return state.user === null
        ? undefined
        : `${state.user.type} ${state.user.access_token}`;
    }
  }
};

export default authStore;
