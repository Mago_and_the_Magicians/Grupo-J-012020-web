import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import i18n from "./i18n";
import store from "./store/store";
import vueAuth from "./plugins/vueAuth";

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  vuetify,
  i18n,
  vueAuth,
  beforeCreate() {
    this.$store.commit("INITIALIZE_ORDER");
    this.$store.commit("INITIALIZE_USER");
  },
  render: h => h(App)
}).$mount("#app");
