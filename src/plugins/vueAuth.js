import Vue from "vue";
import axios from "axios";
import VueAuthenticate from "vue-authenticate";
import VueAxios from "vue-axios";
import Vuetify from "vuetify";

Vue.use(VueAxios, axios);
Vue.use(VueAuthenticate, {
  baseUrl: process.env.VUE_APP_API_URL, // Your API domain

  providers: {
    facebook: {
      clientId: "273481557206717"
    }
  }
});

export default new Vuetify({});
